# A data analysis exercise on Bitcoin

I developed this notebook when I was a zClient Architect in IBM Software Sales Italy, in November 2017, and I used it as a simple demo to show how to work with Data Science Experience on IBM Cloud.

That work merged two of my passions: blockchain and machine learning, so in this project I reviewed and adapted the notebook to be executed from  Jupyter.

Hope this work may be of help for who is beginning his journey to machine learning.

Any comments and suggestions are really appreciated.

## Requirements

* Docker
* Git

## Executing the notebook in a dockerized Jupyter server

```sh
git clone https://gitlab.com/losangeles971/a-data-analysis-exercise-on-bitcoin.git
cd a-data-analysis-exercise-on-bitcoin
docker pull jupyter/tensorflow-notebook
docker run -ti --rm -v $(pwd):/home/jovyan/data -p 8888:8888 jupyter/tensorflow-notebook
```

Once the docker is started, following the instructions to open the Jupyter home page from your browser, then you find the notebook into the directory "data/src/".

## Executing the notebook in a local Jupyter server

If you have a local installation of Jupyter, or you have a local installation of Anaconda, you may:

* Clone this repository into your home directory
* Start Jupyter
* Open your browser to your Jupyter home page
* Navigate where you clone this repository
* Open/edit/execute this notebook